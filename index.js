// 1. Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM):
// Ответ:
// DOM – это представление HTML-документа в виде дерева тегов. В соответствии с объектной моделью документа каждый HTML-тег является объектом. Вложенные теги являются «детьми» родительского элемента. Текст, который находится внутри тега, также является объектом. Теги являются узлами-элементами (или просто элементами) и образуют структуру дерева.


const makeList = (array, parent = document.body) => {
    const result = array.map(item => {
        return `<li>${item}</li>`
    })
    parent.insertAdjacentHTML("afterbegin", `<ul>${result.join(" ")}</ul>`)
}

makeList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"])

